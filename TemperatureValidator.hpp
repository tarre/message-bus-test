#ifndef TEMPERATUREVALIDATOR_HPP
#define TEMPERATUREVALIDATOR_HPP

#include <QObject>

class AppMessageBus;

class TemperatureValidator : public QObject
{
    Q_OBJECT
public:
    explicit TemperatureValidator(AppMessageBus *bus, QObject *parent = nullptr);

signals:

};

#endif // TEMPERATUREVALIDATOR_HPP
