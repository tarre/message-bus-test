#include "NotificationManager.hpp"

#include "AppMessageBus.hpp"

#include <QDebug>

NotificationManager::NotificationManager(AppMessageBus *bus, QObject *parent) : QObject(parent)
{
    setObjectName("NotificationManager");

    connect(bus, &AppMessageBus::HighTemperatureAlert, this, [this](const HighTemperatureAlertMessage& data) {
        QString msg = QString("High temperature %0 reached on %1").arg(data.temperatureReading.timestamp.toString(),
                                                                       data.temperatureReading.sensorId);
        sendNotification(msg);
    });

    //Also send a notification when a user-defined input has changed
//    if (config.userCustomizations.inputsToNotifyOn.size() > 0) {
        connect(bus, &AppMessageBus::InputChanged, this, [this](QString inputId, bool on) {
//            if (config.userCustomizations.inputsToNotifyOn.contains(inputId)) {
                QString msg = QString("Input %0 changed to %1").arg(inputId, QString::number(on));
                sendNotification(msg);
//            }
        });
//    }

//    connect(bus, &AppMessageBus::ExcessiveFailedLoginsAlert, this, [this](const FailedLoginsAlertData& data) {
//        Qstring msg = ...
//        sendNotification(msg);
//    });
}

void NotificationManager::sendNotification(QString msg)
{
    //buffer/batch the sends here
    qDebug() << "NotificationManager sending notification to ADMIN: " << msg;
}
