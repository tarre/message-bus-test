#ifndef APPMESSAGEBUS_HPP
#define APPMESSAGEBUS_HPP

#include <QObject>
#include "CommonTypes.hpp"


//Shared object through which other components raise signals, instead of doing
//local emits. This allows much looser coupling of components, especially when events are emitted at
//a much lower "depth".
class AppMessageBus : public QObject
{
    Q_OBJECT
public:
    explicit AppMessageBus(QObject *parent = nullptr);

signals:
    void NewTemperatureReading(const TemperatureReadingData& reading);
    void HighTemperatureAlert(const HighTemperatureAlertMessage& payload);

    void InputChanged(QString inputId, bool on);

    //...tons of other signals
};

#endif // APPMESSAGEBUS_HPP
