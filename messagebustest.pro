QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

SOURCES += \
        AppMessageBus.cpp \
        CommonTypes.cpp \
        Hardware.cpp \
        InputManager.cpp \
        NotificationManager.cpp \
        ReadingsLogger.cpp \
        Sensor.cpp \
        TemperatureValidator.cpp \
        main.cpp

HEADERS += \
    AppMessageBus.hpp \
    CommonTypes.hpp \
    Hardware.hpp \
    InputManager.hpp \
    NotificationManager.hpp \
    ReadingsLogger.hpp \
    Sensor.hpp \
    TemperatureValidator.hpp
