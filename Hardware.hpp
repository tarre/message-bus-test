#ifndef HARDWARE_HPP
#define HARDWARE_HPP

#include <QObject>
#include "AppMessageBus.hpp"

class Hardware : public QObject
{
    Q_OBJECT
public:
    explicit Hardware(AppMessageBus* bus, QObject *parent = nullptr);

signals:

private:


};

#endif // HARDWARE_HPP
