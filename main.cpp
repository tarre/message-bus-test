#include <QCoreApplication>

#include "AppMessageBus.hpp"
#include "ReadingsLogger.hpp"
#include "TemperatureValidator.hpp"
#include "Hardware.hpp"
#include "NotificationManager.hpp"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    AppMessageBus bus;

    Hardware hw(&bus);
    ReadingsLogger readingsLogger(&bus);
    TemperatureValidator temperatureValidator(&bus);

    NotificationManager manager(&bus);
    //Whatever-New-component component(&bus);

    return a.exec();
}
