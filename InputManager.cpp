#include "InputManager.hpp"

#include "AppMessageBus.hpp"

#include <QDebug>
#include <QTimer>

InputManager::InputManager(AppMessageBus *bus, QObject *parent) : QObject(parent), m_bus(bus)
{
    setObjectName("InputManager");
}

void InputManager::Start()
{
    //Simulate 30% chance of an input every 500ms
    auto timer = new QTimer(this);

    connect(timer, &QTimer::timeout, this, [=](){
        float rand = getRandomFloat(1, 100);
        if (rand < 30) {
            Q_EMIT m_bus->InputChanged("input1", true);
        }
    });

    timer->start(500);
}


