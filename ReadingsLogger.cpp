#include "ReadingsLogger.hpp"

#include "AppMessageBus.hpp"
#include <QDebug>

ReadingsLogger::ReadingsLogger(AppMessageBus *bus, QObject *parent) : QObject(parent)
{
    setObjectName("ReadingsLogger");

    connect(bus, &AppMessageBus::NewTemperatureReading, this, [this](const TemperatureReadingData& reading) {

        //this will be AppMessageBus, not the object that truly emitted the signal using AppMessageBus.
        //Is it possible to get a reference to that object, without embedding as an extra field in the signal
        //signature?
        QObject* sender = this->sender();

        qDebug() << "ReadingsLogger saved normal reading" << reading.reading << "," << reading.timestamp.toString() << "to db. Sender=" << sender->objectName();
    });

    connect(bus, &AppMessageBus::HighTemperatureAlert, this, [](const HighTemperatureAlertMessage& payload) {
        qDebug() << "ReadingsLogger: HIGH TEMPERATURE ALERT saved to db. " << payload.alertMessage;
    });
}
