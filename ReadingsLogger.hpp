#ifndef READINGSLOGGER_HPP
#define READINGSLOGGER_HPP

#include <QObject>

class AppMessageBus;

class ReadingsLogger : public QObject
{
    Q_OBJECT
public:
    explicit ReadingsLogger(AppMessageBus *bus, QObject *parent = nullptr);

signals:

};

#endif // READINGSLOGGER_HPP
