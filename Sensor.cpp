#include "Sensor.hpp"

#include <QTimer>


Sensor::Sensor(QString id, AppMessageBus* bus, QObject *parent) : QObject(parent)
{
    setObjectName(id);

    auto timer = new QTimer(this);

    connect(timer, &QTimer::timeout, this, [=](){
        TemperatureReadingData reading;
        reading.sensorId = id;
        reading.timestamp = QDateTime::currentDateTimeUtc();
        reading.reading = getRandomFloat(-100.0, 100.0);

        Q_EMIT bus->NewTemperatureReading(reading);
    });

    timer->start(2500);
}
