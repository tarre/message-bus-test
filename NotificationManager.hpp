#ifndef NOTIFICATIONMANAGER_HPP
#define NOTIFICATIONMANAGER_HPP

#include <QObject>

class AppMessageBus;

class NotificationManager : public QObject
{
    Q_OBJECT
public:
    explicit NotificationManager(AppMessageBus *bus, QObject *parent = nullptr);

signals:

private:
    void sendNotification(QString msg);

};

#endif // NOTIFICATIONMANAGER_HPP
