#ifndef COMMONTYPES_HPP
#define COMMONTYPES_HPP

#include <QDateTime>
#include <QVariant>

struct TemperatureReadingData {
    QDateTime timestamp;
    QString sensorId;
    double reading;
};

struct HighTemperatureAlertMessage {
    TemperatureReadingData temperatureReading;
    double highLimit;
    QString alertMessage;

    HighTemperatureAlertMessage(const TemperatureReadingData& _reading, double _highLimit, const QString& _alertMessage):
        temperatureReading(_reading), highLimit(_highLimit), alertMessage(_alertMessage)
    {}
};

float getRandomFloat(float min, float max);


#endif // COMMONTYPES_HPP
