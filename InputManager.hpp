#ifndef INPUTMANAGER_HPP
#define INPUTMANAGER_HPP

#include <QObject>

class AppMessageBus;

//Reads hardware inputs such as buttons, GPIOs, etc, and notifies the rest of the application when it happens
//Runs on a separate thread to make this example a bit more complex
class InputManager : public QObject
{
    Q_OBJECT
public:
    explicit InputManager(AppMessageBus *bus, QObject *parent = nullptr);

public slots:
    void Start();

private:
    AppMessageBus* m_bus;
};

#endif // INPUTMANAGER_HPP
