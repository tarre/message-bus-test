#include "TemperatureValidator.hpp"

#include "AppMessageBus.hpp"

#include <QDebug>

TemperatureValidator::TemperatureValidator(AppMessageBus *bus, QObject *parent) : QObject(parent)
{
    setObjectName("TemperatureValidator");

    const double HIGH_TEMP = 60;

    connect(bus, &AppMessageBus::NewTemperatureReading, this, [=](const TemperatureReadingData& reading) {

        if (reading.reading > HIGH_TEMP) {
            QString alertMessage = "High temperature alert: " + QString::number(reading.reading) + " > " + QString::number(HIGH_TEMP);
            HighTemperatureAlertMessage payload(reading, HIGH_TEMP, alertMessage);
            Q_EMIT bus->HighTemperatureAlert(payload);
        }
    });
}
