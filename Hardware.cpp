#include "Hardware.hpp"

#include <Sensor.hpp>
#include <InputManager.hpp>

#include <QDebug>
#include <QThread>
#include <QTimer>

Hardware::Hardware(AppMessageBus* bus, QObject *parent) : QObject(parent)
{
    setObjectName("Hardware");

    for (int i = 0; i < 10; i++) {
       auto sensor = new Sensor("sensor" + QString::number(i), bus);
       qDebug() << "Created sensor" << sensor->objectName();
    }

    auto input = new InputManager(bus);
    auto inputThread = new QThread(this);
    input->moveToThread(inputThread);
    inputThread->start();
    QTimer::singleShot(0, input, &InputManager::Start);
}
