#ifndef SENSOR_HPP
#define SENSOR_HPP

#include <QObject>

#include "AppMessageBus.hpp"

class Sensor : public QObject
{
    Q_OBJECT
public:
    explicit Sensor(QString id, AppMessageBus* bus, QObject *parent = nullptr);
};

#endif // SENSOR_HPP
